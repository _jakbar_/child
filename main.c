#include <stdio.h>
#include <stdlib.h>

#include "add.h"

int main()
{
	FILE *filePointerA, *filePointerB;
	char a, b; 
	int sum;
	
	filePointerA = fopen("valueA.txt", "r");
	filePointerB = fopen("valueB.txt", "r");
	
	if((filePointerA == NULL) || (filePointerB == NULL))
	{
		printf("Can't locate input files!");
		exit(EXIT_FAILURE);
	}
	a = fgetc(filePointerA);
	b = fgetc(filePointerB);
	
	a = atoi(&a);
	b = atoi(&b);	
	sum = sum_of_two_numbers(a,b);

	printf("Sum is %i\n", sum);

	return 0;
}
